﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _2task
{
    internal class File
    {
        public static void CreateTextFile(string fileName, DataGridView grid)
        {
            StreamWriter sw = new StreamWriter(fileName);
            for (int i = 0; i < grid.ColumnCount; i++)
            {
                sw.WriteLine(grid[i, 0].Value);
            }
            sw.Close();
        }
        public static double[]  readFromFile(string fileName)
        {
            StreamReader sr = new StreamReader(fileName);
            
            int columnsCount = Convert.ToInt32(sr.ReadLine());
            double[] vect = new double[ columnsCount];
           
                for (int j = 0; j < columnsCount; j++)
              {
                    vect[j] = Convert.ToDouble(sr.ReadLine());
                }
           
            sr.Close();
            return vect;
        }
        


        public static void CreateDataFile(string fileName, DataGridView grid)
        {
            FileStream fs = new FileStream(fileName, FileMode.Create);
            BinaryWriter bw = new BinaryWriter(fs);
            for (int i = 0; i < grid.ColumnCount; i++)
            {
                bw.Write(Convert.ToInt32(grid[i, 0].Value));
            }
            bw.Close();
            fs.Close();
        }
        public static void ReadDataFile(string fileName, DataGridView grid)
        {
            FileStream FS1 = new FileStream(fileName, FileMode.Open);
            BinaryReader BR = new BinaryReader(FS1);
           
            double d2 = 1;
            while (FS1.Position < FS1.Length)
{
                double d = BR.ReadDouble();
                if (d <  0)
{
                    d2 = d2 * d;
                }
            }
        }
    }
}
