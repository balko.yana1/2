﻿namespace _2task
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.label1 = new System.Windows.Forms.Label();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.вийтиToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.файлToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.зберегтиВТекстовийToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.зберегтиВБінарнийToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.зверегтиВExcelToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.завантажитиТекстовийToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.завантажитиБінарнийToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.діїToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.хToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.загальнаВартістьToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(67, 124);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowHeadersWidth = 51;
            this.dataGridView1.RowTemplate.Height = 24;
            this.dataGridView1.Size = new System.Drawing.Size(448, 189);
            this.dataGridView1.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(4, 42);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(0, 16);
            this.label1.TabIndex = 2;
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.вийтиToolStripMenuItem,
            this.файлToolStripMenuItem,
            this.діїToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(800, 28);
            this.menuStrip1.TabIndex = 3;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // вийтиToolStripMenuItem
            // 
            this.вийтиToolStripMenuItem.Name = "вийтиToolStripMenuItem";
            this.вийтиToolStripMenuItem.Size = new System.Drawing.Size(64, 24);
            this.вийтиToolStripMenuItem.Text = "вийти";
            // 
            // файлToolStripMenuItem
            // 
            this.файлToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.зберегтиВТекстовийToolStripMenuItem,
            this.зберегтиВБінарнийToolStripMenuItem,
            this.зверегтиВExcelToolStripMenuItem,
            this.завантажитиТекстовийToolStripMenuItem,
            this.завантажитиБінарнийToolStripMenuItem});
            this.файлToolStripMenuItem.Name = "файлToolStripMenuItem";
            this.файлToolStripMenuItem.Size = new System.Drawing.Size(59, 24);
            this.файлToolStripMenuItem.Text = "Файл";
            // 
            // зберегтиВТекстовийToolStripMenuItem
            // 
            this.зберегтиВТекстовийToolStripMenuItem.Name = "зберегтиВТекстовийToolStripMenuItem";
            this.зберегтиВТекстовийToolStripMenuItem.Size = new System.Drawing.Size(254, 26);
            this.зберегтиВТекстовийToolStripMenuItem.Text = "зберегти в текстовий";
            this.зберегтиВТекстовийToolStripMenuItem.Click += new System.EventHandler(this.зберегтиВТекстовийToolStripMenuItem_Click);
            // 
            // зберегтиВБінарнийToolStripMenuItem
            // 
            this.зберегтиВБінарнийToolStripMenuItem.Name = "зберегтиВБінарнийToolStripMenuItem";
            this.зберегтиВБінарнийToolStripMenuItem.Size = new System.Drawing.Size(254, 26);
            this.зберегтиВБінарнийToolStripMenuItem.Text = "зберегти  в бінарний";
            this.зберегтиВБінарнийToolStripMenuItem.Click += new System.EventHandler(this.зберегтиВБінарнийToolStripMenuItem_Click);
            // 
            // зверегтиВExcelToolStripMenuItem
            // 
            this.зверегтиВExcelToolStripMenuItem.Name = "зверегтиВExcelToolStripMenuItem";
            this.зверегтиВExcelToolStripMenuItem.Size = new System.Drawing.Size(254, 26);
            this.зверегтиВExcelToolStripMenuItem.Text = "зверегти в excel";
            this.зверегтиВExcelToolStripMenuItem.Click += new System.EventHandler(this.зверегтиВExcelToolStripMenuItem_Click);
            // 
            // завантажитиТекстовийToolStripMenuItem
            // 
            this.завантажитиТекстовийToolStripMenuItem.Name = "завантажитиТекстовийToolStripMenuItem";
            this.завантажитиТекстовийToolStripMenuItem.Size = new System.Drawing.Size(254, 26);
            this.завантажитиТекстовийToolStripMenuItem.Text = "завантажити текстовий";
            this.завантажитиТекстовийToolStripMenuItem.Click += new System.EventHandler(this.завантажитиТекстовийToolStripMenuItem_Click);
            // 
            // завантажитиБінарнийToolStripMenuItem
            // 
            this.завантажитиБінарнийToolStripMenuItem.Name = "завантажитиБінарнийToolStripMenuItem";
            this.завантажитиБінарнийToolStripMenuItem.Size = new System.Drawing.Size(254, 26);
            this.завантажитиБінарнийToolStripMenuItem.Text = "завантажити бінарний";
            this.завантажитиБінарнийToolStripMenuItem.Click += new System.EventHandler(this.завантажитиБінарнийToolStripMenuItem_Click);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // діїToolStripMenuItem
            // 
            this.діїToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.хToolStripMenuItem,
            this.загальнаВартістьToolStripMenuItem});
            this.діїToolStripMenuItem.Name = "діїToolStripMenuItem";
            this.діїToolStripMenuItem.Size = new System.Drawing.Size(39, 24);
            this.діїToolStripMenuItem.Text = "дії";
            // 
            // хToolStripMenuItem
            // 
            this.хToolStripMenuItem.Name = "хToolStripMenuItem";
            this.хToolStripMenuItem.Size = new System.Drawing.Size(224, 26);
            this.хToolStripMenuItem.Text = "х";
            this.хToolStripMenuItem.Click += new System.EventHandler(this.хToolStripMenuItem_Click);
            // 
            // загальнаВартістьToolStripMenuItem
            // 
            this.загальнаВартістьToolStripMenuItem.Name = "загальнаВартістьToolStripMenuItem";
            this.загальнаВартістьToolStripMenuItem.Size = new System.Drawing.Size(224, 26);
            this.загальнаВартістьToolStripMenuItem.Text = "загальна вартість";
            this.загальнаВартістьToolStripMenuItem.Click += new System.EventHandler(this.загальнаВартістьToolStripMenuItem_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem вийтиToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem файлToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem зберегтиВТекстовийToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem зберегтиВБінарнийToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem зверегтиВExcelToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem завантажитиТекстовийToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem завантажитиБінарнийToolStripMenuItem;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.ToolStripMenuItem діїToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem хToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem загальнаВартістьToolStripMenuItem;
    }
}

