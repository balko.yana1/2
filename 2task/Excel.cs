﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Excel = Microsoft.Office.Interop.Excel;
namespace _2task
{
    internal class Excel1
    {
        public static void saveToExcel(DataGridView grid, string fileName, string sheetName)
        {

            Excel.Application app = new Microsoft.Office.Interop.Excel.Application();
            app.Visible = true;
            Excel.Workbook item= app.Workbooks.Add(Type.Missing);
            app.SheetsInNewWorkbook = 1;
            app.DisplayAlerts = false;
            Excel.Worksheet sheet = (Excel.Worksheet)app.Worksheets.get_Item(1);
            sheet.Name = sheetName;
            int row = 1;

            for (int j = 0; j < grid.ColumnCount; j++)
            {
                sheet.Cells[row, j + 1] = grid.Columns[j].HeaderText;
            }
            for (int i = 0; i < grid.RowCount; i++)
            {
                for (int j = 0; j < grid.ColumnCount; j++)
                {
                    sheet.Cells[row + i + 1, j + 1] = grid[j, i].Value;
                }
            }
            Excel.Range range = sheet.Range[sheet.Cells[row, 1], sheet.Cells[row, grid.ColumnCount]];
            
            app.Application.ActiveWorkbook.SaveAs($"{fileName}.xlsx");
            item.Close();
            app.Quit();
        }
    }
}
