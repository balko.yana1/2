﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _2task
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            dataGridView1.RowCount = 1;
            dataGridView1.ColumnCount = 5;
            

            dataGridView1.Columns[0].HeaderText = "Код";
            dataGridView1.Columns[1].HeaderText = "Назва товару";
            dataGridView1.Columns[2].HeaderText = "Країна";
            dataGridView1.Columns[3].HeaderText = "Об'єм";
            dataGridView1.Columns[4].HeaderText = "Ціна";

        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void зверегтиВExcelToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                Excel1.saveToExcel(dataGridView1, saveFileDialog1.FileName, openFileDialog1.FileName);

            }
        }

        private void зберегтиВТекстовийToolStripMenuItem_Click(object sender, EventArgs e)
        {
            saveFileDialog1.Filter = "txt files (*.txt)|*.txt|dat files (*.dat)|*.dat";
            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {

                File.CreateTextFile(saveFileDialog1.FileName, dataGridView1);



            }
        }

        private void зберегтиВБінарнийToolStripMenuItem_Click(object sender, EventArgs e)
        {
            saveFileDialog1.Filter = "txt files (*.txt)|*.txt|dat files (*.dat)|*.dat";
            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {



                File.CreateDataFile(saveFileDialog1.FileName, dataGridView1);

            }
        }

        private void завантажитиТекстовийToolStripMenuItem_Click(object sender, EventArgs e)
        {
            
            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {



                File.CreateTextFile(openFileDialog1.FileName, dataGridView1);

            }
        }

        private void завантажитиБінарнийToolStripMenuItem_Click(object sender, EventArgs e)
        {
           
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {



                File.CreateDataFile(saveFileDialog1.FileName, dataGridView1);

            }
        }

        private void хToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string name = dataGridView1.Columns[1].ValueType.ToString();
            string value = dataGridView1.Columns[3].ValueType.ToString();
            MessageBox.Show("name=" + name + "value=" + value);
        }
        
        private void загальнаВартістьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            double[,] data = new double[dataGridView1.RowCount,dataGridView1.ColumnCount];
            double num = 0;
            for (int i = 0; i < dataGridView1.Rows.Count; i++)
            {
                for (int j = 0; j < dataGridView1.ColumnCount; j++) {
                    data[j, i] = (double)dataGridView1[i, j].Value;
                    num += data[4, i];
                }
            }
            MessageBox.Show(num.ToString());
        }
    }
}
