﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MKR_2
{
   
    internal class Vector
    {
        double[] data;
        public Vector(int length)
        {
            data = randomVector(length);
        }
        public Vector(DataGridView grid)
        {
            data = readFromGrid(grid);

        }
        public double[] randomVector (int colNum)
        {
            double[] vect = new double[colNum];
            Random rand = new Random(DateTime.Now.Millisecond);
            for(int j =0; j<colNum; j++)
            {
                vect[j] = rand.Next(-50, 50);
            }
            return vect;

        }
        public static double[] readFromGrid (DataGridView grid)
        {
            double[] vect = new double[grid.Columns.Count];
            for(int j = 0; j < grid.Columns.Count; j++)
            {
                vect[j] = Convert.ToDouble(grid[j, 0].Value);
            }
            return vect;
        }
        public void writeToGrid(DataGridView grid)
        {

            grid.ColumnCount = data.Length;
            for (int j = 0; j < data.Length; j++)
            {
                {
                    grid[j, 0].Value = data[j];
                }
            }
        }
        public static double minValue(DataGridView grid)
        {
            double[] vect = Vector.readFromGrid(grid);
            double maxValue = 0;
            double[] min = new double[grid.Columns.Count];
            for (int j = 0; j < grid.Columns.Count; j++)
            {
                if (vect[j] > 0)
                {
                    min[j] = vect[j];
                    maxValue = min.Min();
                }

            }
            return maxValue;
        }
        public static double maxValue(DataGridView grid)
        {
           
            double[] vect = Vector.readFromGrid(grid);
            double minValue = 0;
            double[] max = new double[grid.Columns.Count];
            for (int j = 0; j < grid.Columns.Count; j++)
            {
                if (vect[j] < 0)
                {
                    max[j] = vect[j];
                    minValue = max.Max();
                }

            }
            return minValue;
        }


    }
}
