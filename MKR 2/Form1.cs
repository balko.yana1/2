﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MKR_2
{
    public partial class Form1 : Form
    {
        Vector vect;
        public Form1()
        {
            InitializeComponent();
            dataGridView1.RowCount = 1;
            dataGridView1.ColumnCount = 2;

           
        }

        private void вийтиToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void numericUpDown1_ValueChanged(object sender, EventArgs e)
        {
            dataGridView1.ColumnCount =(int) numericUpDown1.Value;
        }

        private void згенеруватиToolStripMenuItem_Click(object sender, EventArgs e)
        {
            vect = new Vector(dataGridView1.ColumnCount);
            vect.writeToGrid(dataGridView1);
        }

        private void знайтиНайбільшеСередДодатніхToolStripMenuItem_Click(object sender, EventArgs e)
        {
            double max = Vector.maxValue(dataGridView1);
            MessageBox.Show("max=" + max);

        }

        private void знайтиНайToolStripMenuItem_Click(object sender, EventArgs e)
        {
            double min = Vector.minValue(dataGridView1);
            MessageBox.Show("mix=" + min);

        }
    }
}
